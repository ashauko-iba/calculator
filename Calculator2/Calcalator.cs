﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator2
{
    public class Calculator
    {
        public static double Calculate(string input)
        {
            string output = GetExpression(input);
            double result = Solution(output);
            return result;
        }

        private static bool IsOperator(char s)
        {
            if ("/*-+^".Contains(s))
                return true;
            return false;
        }
        private static string GetExpression(string input)
        {
            string[] output = new string[0];
            string[] stack = new string[0];
            string permitted = "1234567890-+/*^()";
            int k = 0;
            int count = 0;
            bool minus = false;
            for (int i = 0; i < input.Length; i++)
            {
                string temp = "";
                if (!permitted.Contains(input[i]))
                    throw new LogicException("Выражение содержит недопустимые символы");
                if (input[i] == '-' && ((i > 0 && !Char.IsDigit(input[i - 1]) && input[i-1]!=')') || i == 0) && input[i + 1] != '(')
                {
                    if (i > 0 && input[i] == '-' && (input[i - 1] == '/' || input[i - 1] == '*'))
                        throw new LogicException("Выражение нелогично");
                    i++;
                    temp += "-";
                    //Array.Resize(ref stack, stack.Length + 1);
                    //stack[stack.Length - 1] = "+";
                }
                if (input[i] == '+' && ((i > 0 && !Char.IsDigit(input[i - 1]) && input[i-1]!=')') || i == 0) && input[i + 1] != '(')
                {
                    if (i > 0 && input[i] == '+' && (input[i - 1] == '/' || input[i - 1] == '*'))
                        throw new LogicException("Выражение нелогично");
                    i++;
                    temp += "+";
                }
                if (Char.IsDigit(input[i]))
                {
                    while (i < input.Length && Char.IsDigit(input[i]))
                        temp += input[i++].ToString();
                    i--;
                    Array.Resize(ref output, output.Length + 1);
                    output[output.Length - 1] = temp;
                }
                if (input[i] == '+' || input[i] == '-')
                {
                    if (i == 0)
                        continue;
                m:
                    if (i + 2 < input.Length)
                    {
                        if (IsOperator(input[i + 1]) && IsOperator(input[i + 2]))
                            throw new LogicException("Выражение содержит нелогичные операции");
                    }
                    if (stack.Length != 0)
                    {
                        if (stack[stack.Length - 1] == "(")
                        {
                            Array.Resize(ref stack, stack.Length + 1);
                            stack[stack.Length - 1] = input[i].ToString();
                        }
                        else
                        {
                            Array.Resize(ref output, output.Length + 1);
                            output[output.Length - 1] = stack[stack.Length - 1];
                            Array.Resize(ref stack, stack.Length - 1);
                            goto m;
                        }
                        stack[stack.Length - 1] = input[i].ToString();
                    }
                    else
                    {
                        Array.Resize(ref stack, stack.Length + 1);
                        stack[stack.Length - 1] = input[i].ToString();
                    }
                }
                if (input[i] == '*' || input[i] == '/')
                {
                    if (i == 0)
                        continue;
                    if (i + 1 < input.Length)
                    {
                        if (IsOperator(input[i + 1]))
                            throw new LogicException("Выражение содержит нелогичные операции");
                    }
                    if (stack.Length != 0)
                    {
                        if (stack[stack.Length - 1] != "*" && stack[stack.Length - 1] != "/")
                        {
                            Array.Resize(ref stack, stack.Length + 1);
                            stack[stack.Length - 1] = input[i].ToString();
                        }
                        else
                        {
                            Array.Resize(ref output, output.Length + 1);
                            output[output.Length - 1] = stack[stack.Length - 1];
                            stack[stack.Length - 1] = input[i].ToString();
                        }
                    }
                    else
                    {
                        Array.Resize(ref stack, stack.Length + 1);
                        stack[stack.Length - 1] = input[i].ToString();
                    }
                }
                if (input[i] == '(')
                {
                    if (i > 0 && input[i-1] == ')')
                        throw new LogicException("Выражение нелогично");
                    if (i + 1 < input.Length && input[i + 1] == ')')
                        throw new LogicException("Выражение имеет проблемы со скобками");
                    if (i > 0)
                    {
                        if (i > 1)
                        {
                            if (input[i - 1] == '-' && !Char.IsDigit(input[i - 2]))
                                minus = !minus;
                        }
                        else if (input[i - 1] == '-')
                            minus = !minus;
                    }
                    count++;
                    Array.Resize(ref stack, stack.Length + 1);
                    stack[stack.Length - 1] = input[i].ToString();
                }
                if (input[i] == ')')
                {
                    if (count == 0)
                        throw new LogicException("В выражении содержится ошибка, связанная со скобками");
                    count--;
                    int index = stack.Length - 1;
                    while (stack[index] != "(")
                        index--;
                    int j;
                    for (j=index+1; j < stack.Length; j++)
                    {
                        Array.Resize(ref output, output.Length + 1);
                        output[output.Length - 1] = stack[j];
                    }
                    if (index > 0)
                        j--;
                    if (minus)
                    {
                        Array.Resize(ref output, output.Length + 1);
                        output[output.Length - 1] = "m";
                        minus = false;
                    }
                    //j--;
                    Array.Resize(ref stack, (stack.Length - j));
                }
                if (input[i] == '^')
                {
                    Array.Resize(ref stack, stack.Length + 1);
                    stack[stack.Length - 1] = input[i].ToString();
                }
            }
            if (count != 0)
                throw new LogicException("В выражении содержится ошибка, связанная со скобками");
            Array.Resize(ref output, output.Length + stack.Length);
            Array.Reverse(stack);
            for (int i = output.Length - stack.Length; i < output.Length; i++)
                output[i] = stack[k++];
            string result = "";
            for (int i = 0; i < output.Length; i++)
            {
                if (i != output.Length - 1)
                    result += output[i] + " ";
                else
                    result += output[i];
            }
            return result;
        }
        private static double Solution(string input)
        {
            string[] arr = input.Split(' ');
            string temp;
            for (int i = 0; i < arr.Length; i++)
            {
                switch (arr[i])
                {
                    case "+":
                        temp = (double.Parse(arr[i - 2]) + double.Parse(arr[i - 1])).ToString();
                        arr[i - 2] = temp;
                        for (int j = i - 1; j < arr.Length - 2; j++)
                            arr[j] = arr[j + 2];
                        Array.Resize(ref arr, arr.Length - 2);
                        i -= 2;
                        break;
                    case "-":
                        temp = (double.Parse(arr[i - 2]) - double.Parse(arr[i - 1])).ToString();
                        arr[i - 2] = temp;
                        for (int j = i - 1; j < arr.Length - 2; j++)
                            arr[j] = arr[j + 2];
                        Array.Resize(ref arr, arr.Length - 2);
                        i -= 2;
                        break;
                    case "m":
                        temp = (0 - double.Parse(arr[i - 1])).ToString();
                        arr[i - 1] = temp;
                        for (int j = i; j < arr.Length - 1; j++)
                            arr[j] = arr[j + 1];
                        Array.Resize(ref arr, arr.Length - 1);
                        i--;
                        break;
                    case "*":
                        temp = (double.Parse(arr[i - 2]) * double.Parse(arr[i - 1])).ToString();
                        arr[i - 2] = temp;
                        for (int j = i - 1; j < arr.Length - 2; j++)
                            arr[j] = arr[j + 2];
                        Array.Resize(ref arr, arr.Length - 2);
                        i -= 2;
                        break;
                    case "/":
                        if (double.Parse(arr[i - 1]) == 0)
                            throw new LogicException("Деление на ноль");
                        temp = (double.Parse(arr[i - 2]) / double.Parse(arr[i - 1])).ToString();
                        arr[i - 2] = temp;
                        for (int j = i - 1; j < arr.Length - 2; j++)
                            arr[j] = arr[j + 2];
                        Array.Resize(ref arr, arr.Length - 2);
                        i -= 2;
                        break;
                    case "^":
                        temp = (Math.Pow(double.Parse(arr[i - 2]), double.Parse(arr[i - 1]))).ToString();
                        arr[i - 2] = temp;
                        for (int j = i - 1; j < arr.Length - 2; j++)
                            arr[j] = arr[j + 2];
                        Array.Resize(ref arr, arr.Length - 2);
                        i -= 2;
                        break;
                }
            }
            return double.Parse(arr[0]);
        }
    }
    public class LogicException : Exception
    {
        public LogicException(string message)
            : base(message)
        { }
    }
}
